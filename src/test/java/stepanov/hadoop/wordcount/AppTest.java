/* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stepanov.hadoop.wordcount;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class AppTest {
	/*
	 * Declare harnesses that let you test a mapper, a reducer, and a mapper and
	 * a reducer working together.
	 */
	MapDriver<Object, Text, Text, IntWritable> mapDriver;
	ReduceDriver<Text, IntWritable, Text, Text> reduceDriver;
	MapReduceDriver<Object, Text, Text, IntWritable, Text, Text> mapReduceDriver;

	@Before
	public void setUp() throws Exception {
		/*
		 * Set up the mapper test harness.
		 */
		WordCount.TokenizerMapper mapper = new WordCount.TokenizerMapper();
		mapDriver = new MapDriver<Object, Text, Text, IntWritable> ();
		mapDriver.setMapper(mapper);

		/*
		 * Set up the reducer test harness.
		 */
		WordCount.IntSumReducer reducer = new WordCount.IntSumReducer();
		reduceDriver = new ReduceDriver<Text, IntWritable, Text, Text> ();
		reduceDriver.setReducer(reducer);

		/*
		 * Set up the mapper/reducer test harness.
		 */
		mapReduceDriver = new MapReduceDriver<Object, Text, Text, IntWritable, Text, Text>();
		mapReduceDriver.setMapper(mapper);
		mapReduceDriver.setReducer(reducer);
	}

	/* 
	 * Test the mapper.
	 */
	@Test
	public void testMapper() throws IOException {

		String inputString = new String(Files.readAllBytes(Paths.get("/home/cloudera/workspace/wordcount/LogDataShort/test")));
		//		System.out.println(inputString);
		mapDriver.withInput(new LongWritable(), new Text(inputString));

		mapDriver.withOutput(new Text("209.185.108.134"), new IntWritable(4263));
		mapDriver.withOutput(new Text("109.169.248.247"), new IntWritable(4494));
		mapDriver.withOutput(new Text("46.72.177.4"), new IntWritable(4263));
		mapDriver.withOutput(new Text("46.72.177.4"), new IntWritable(4494));

		/*
		 * Run the test.
		 */
		mapDriver.runTest();
	}

	/*
	 * Test the reducer.
	 */
	@Test
	public void testReducer() throws IOException {
		reduceDriver.withCacheArchive("KnownAddress");

		List<IntWritable> values = new ArrayList<IntWritable>();
		values.add(new IntWritable(4264));	

		reduceDriver.withInput(new Text("209.185.108.100"), values);
		reduceDriver.withOutput(new Text("209.185.108.100"), new Text("4264\t4264"));		

		values.add(new IntWritable(4494));

		reduceDriver.withInput(new Text("209.185.108.134"), values);
		reduceDriver.withOutput(new Text("Google.com"), new Text("8758\t4379"));

		reduceDriver.withInput(new Text("217.69.134.168"), values);
		reduceDriver.withOutput(new Text("Mail.ru"), new Text("8758\t4379"));

		reduceDriver.withInput(new Text("194.0.131.176"), values);
		reduceDriver.withOutput(new Text("Meta.com"), new Text("8758\t4379"));

		reduceDriver.withInput(new Text("207.46.13.132"), values);
		reduceDriver.withOutput(new Text("MSN.com"), new Text("8758\t4379"));

		reduceDriver.withInput(new Text("81.19.66.90"), values);
		reduceDriver.withOutput(new Text("Rambler.ru"), new Text("8758\t4379"));

		reduceDriver.withInput(new Text("67.195.37.172"), values);
		reduceDriver.withOutput(new Text("Yahoo.com"), new Text("8758\t4379"));

		reduceDriver.withInput(new Text("213.180.194.137"), values);
		reduceDriver.withOutput(new Text("Yandex.ru"), new Text("8758\t4379"));

		/*
		 * Run the test.
		 */
		reduceDriver.runTest();
	}

	/*
	 * Test the mapper and reducer working together.
	 */
	@Test
	public void testMapReduce() throws IOException {
		
		mapReduceDriver.withCacheArchive("KnownAddress");

		String inputStr = new String(Files.readAllBytes(Paths.get("/home/cloudera/workspace/wordcount/LogDataShort/test")));
		mapReduceDriver.withInput(new LongWritable(), new Text(inputStr));
		
		mapReduceDriver.withOutput(new Text("109.169.248.247"), new Text("4494\t4494"));
		mapReduceDriver.withOutput(new Text("Google.com"), new Text("4263\t4263"));
		mapReduceDriver.withOutput(new Text("46.72.177.4"), new Text("8757\t4378"));

		/*
		 * Run the test.
		 */
		mapReduceDriver.runTest();
	}    
}
