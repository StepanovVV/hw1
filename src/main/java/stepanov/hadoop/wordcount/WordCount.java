package stepanov.hadoop.wordcount;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException; 
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer; 

import org.apache.commons.lang.math.NumberUtils;
import org.apache.hadoop.conf.Configuration; 
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path; 
import org.apache.hadoop.io.IntWritable; 
import org.apache.hadoop.io.Text; 
import org.apache.hadoop.mapreduce.Job; 
import org.apache.hadoop.mapreduce.Mapper; 
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.filecache.DistributedCache;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat; 
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.http.conn.util.InetAddressUtils;

/**
 * WordCount - counts length of data words in IP requests/responses
 * 
 * @author cloudera
 *
 */
@SuppressWarnings("deprecation")
public class WordCount 
{
	/**
	 * 
	 * @author cloudera
	 *
	 */
	public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable>{

		private StringTokenizer itr;
		private StringTokenizer sItr;
		private Text ip = new Text(); 
		private String ipStr;
		private boolean isIP;
		private Integer bytes;
		private String bytesStr;
		private boolean isBytes;
		private String tempStr;
		private int i;

		@Override
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

			sItr = new StringTokenizer(value.toString(), "\n");

			while(sItr.hasMoreTokens()){

				itr = new StringTokenizer(sItr.nextToken()); 
				i = 0;

				// проверяем что в строке есть токены
				while (itr.hasMoreElements()) {
//					i = 0		//  хост/IP-адрес, с которого произведён запрос к серверу;
//					i = 1		// -
//					i = 2		// -
//					i = 3		// время запроса к серверу
//					i = 4		// и часовой пояс сервера;
//					i = 5		// тип запроса
//					i = 6		// его содержимое
//					i = 7		// и версия;
//					i = 8		// код состояния HTTP
//					i = 9		// количество отданных сервером байт;
					
					tempStr = itr.nextToken();	
					
					if (i == 0) {
						ipStr = tempStr;						
						ip.set(ipStr);									
					}
					if (i == 9) {
						bytesStr = tempStr;						

						isIP = InetAddressUtils.isIPv4Address(ipStr);
						isBytes = NumberUtils.isNumber(bytesStr);

						// фильтруем строки, в которых нет адреса или длины запроса
						if (isIP && isBytes) {
							bytes = Integer.parseInt(bytesStr);

							context.write(ip, new IntWritable(bytes));
						}
						break;
					}
					
					i++;
				}
			}
		} 		
	} 

	/**
	 * 
	 * @author cloudera
	 *
	 * 
	 */
	public static class IntSumReducer extends Reducer<Text,IntWritable,Text,Text> { 
		private Text result = new Text(); 
		private List<KnownData> knownDatas = new ArrayList<KnownData>();
		String string;
		private int sum, i;

		/**
		 * Structure, contents resource name and it's IP addresses
		 * @author cloudera
		 *
		 */
		protected class KnownData{
			public String resourceName;
			public Set<String> knownAdresses = new HashSet<String>();
		}

		@Override
		protected void setup(Context context) throws IOException, InterruptedException {

			// read all files in distributed cache
			LocalFileSystem localFileSystem = FileSystem.getLocal(context.getConfiguration());

			// convert Path to File to search for files in catalog
			Path[] folderPath = DistributedCache.getLocalCacheArchives(context.getConfiguration());
			if (folderPath != null && folderPath.length > 0) {
				for (Path path : folderPath) {

					File folder = new File(localFileSystem.pathToFile(path).getAbsolutePath());

					for (File file : folder.listFiles()) {
						if (file.isFile()) {
							readFile(file);					
						}
					}
				}
			}
		}

		/**
		 * 
		 * Method saves list if known IP addresses
		 * 
		 * @param file - file being processed
		 * @throws IOException
		 */
		private void readFile(File file) throws IOException {
			KnownData localData = new KnownData();
			String IP;

			// read file with addresses and sort them by file name
			@SuppressWarnings("resource")
			BufferedReader bufferedReader = new BufferedReader(new FileReader(file.getAbsolutePath().toString()));
			while ((IP = bufferedReader.readLine()) != null) {
				localData.knownAdresses.add(IP);
			}
			localData.resourceName = file.getName();
			knownDatas.add(localData);
		}

		@Override
		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException { 
			sum = 0; 
			i = 0;

			for (IntWritable val : values) { 
				sum += val.get(); 
				i++;
			} 

			// create string with average bytes per request and total bytes
			string = Integer.toString(sum);
			string += "\t";
			string += Integer.toString(sum / i);
			result.set(string);

			// replacement of known addresses
			for (KnownData knownData : knownDatas) {
				if (knownData.knownAdresses.contains(key.toString())) {
					key.set(knownData.resourceName);
					break;
				}
			}
			context.write(key, result); 
		} 
	} 

	/**
	 * @param args
	 * @throws Exception
	 * 
	 * Main method. Configures job and starts it.
	 */
	public static void main(String[] args) throws Exception {
		Job job = Job.getInstance(new Configuration(), "word count");

		// Attach distributed cache files
		DistributedCache.addCacheArchive(new Path("KnownAddress").toUri(), job.getConfiguration());

		job.setJarByClass(WordCount.class); 
		job.setMapperClass(TokenizerMapper.class); 
		job.setReducerClass(IntSumReducer.class); 
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setOutputKeyClass(Text.class); 
		job.setOutputValueClass(Text.class); 
		job.setOutputFormatClass(SequenceFileOutputFormat.class);
		FileInputFormat.addInputPath(job, new Path("./LogDataALL")); 
		SequenceFileOutputFormat.setOutputPath(job, new Path("hdfs://localhost/user/cloudera/LogData_Count.seq"));

		System.exit(job.waitForCompletion(true) ? 0 : 1); 
	} 
}
